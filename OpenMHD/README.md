MHD code for ISSS-15
====================

This MHD code is a subset of OpenMHD code,
available at the following URLs.

 * <https://sci.nao.ac.jp/MEMBER/zenitani/openmhd-e.html>
 * <https://sci.nao.ac.jp/MEMBER/zenitani/openmhd-j.html>
